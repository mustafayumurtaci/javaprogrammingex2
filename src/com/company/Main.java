package com.company;

public class Main {

    public static void main(String[] args) {
        Account accounts[] = new Account[2];
        accounts[0] = new Corporation("Google");
        accounts[1] = new Individual("Mustafa");
        System.out.println(accounts[0].usage());
        System.out.println(accounts[1].usage());
    }
}
