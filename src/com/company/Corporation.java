package com.company;

public class Corporation extends Account {

    public Corporation(String owner) {
        super(owner);
        setType("corporation");

    }

    @Override
    public String usage() {
        return getOwner() + " Corporation: The " + getType() + " account is open";
    }

}
