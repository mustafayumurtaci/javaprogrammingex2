package com.company;

public abstract class Account implements Cloneable {
    private String owner;
    private String type;

    public Account(String owner) {
        this.owner = owner;
    }


    public String getOwner() {
        return owner;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public abstract String usage();
}
