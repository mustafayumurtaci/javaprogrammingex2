package com.company;

public class Individual extends Account {

    public Individual(String owner) {
    super(owner);
    setType("individual");
    }

    @Override
    public String usage() {
        return getOwner() + "'s account: The " + getType() + " account is open";

    }

}
